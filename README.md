# Examen 1: Un juego de Plataformas
## Proyecto: un juego de plataformas

>Toda la realidad es un juego. —Iain Banks

Gran parte de mi fascinación inicial por los ordenadores, como la de muchos niños nerds, tenía que ver con los videojuegos.

Me sentí atraído por los diminutos mundos simulados que podía manipular y en los que las historias se desarrollaban (más o menos), supongo, más por la forma en que proyectaba mi imaginación en ellos que por las posibilidades que realmente ofrecían.

No le deseo a nadie una carrera en programación de juegos. Al igual que en la industria de la música, la discrepancia entre el número de jóvenes deseosos de trabajar en ella y la demanda real de esas personas crea un entorno bastante poco saludable. Pero escribir juegos por diversión es divertido.

Los juegos de plataformas (o juegos de “saltar y correr”) son juegos que esperan que el jugador mueva una figura a través de un mundo, que generalmente es bidimensional y se ve de lado, mientras salta sobre cosas.

### El juego
Nuestro juego se basará aproximadamente en **Dark Blue** de *Thomas Palef*. 

Elegí ese juego porque es entretenido y minimalista y porque se puede construir sin demasiado código. Se parece a esto:

El juego **Dark Blue**
La caja oscura representa al jugador, cuya tarea es recolectar las cajas amarillas (monedas) mientras evita la materia roja (lava). Se completa un nivel cuando se han recolectado todas las monedas.

El jugador puede caminar con las teclas de flecha izquierda y derecha y puede saltar con la flecha hacia arriba. Saltar es una especialidad de este personaje del juego. Puede alcanzar varias veces su propia altura y cambiar de dirección en el aire. Puede que esto no sea del todo realista, pero ayuda a que el jugador tenga la sensación de tener el control directo del avatar en pantalla.

El juego consiste en un fondo estático, dispuesto como una cuadrícula, con los elementos móviles superpuestos sobre ese fondo. Cada campo de la cuadrícula está vacío, sólido o de lava. Los elementos móviles son el jugador, las monedas y ciertas piezas de lava. Las posiciones de estos elementos no están restringidas a la cuadrícula; sus coordenadas pueden ser fraccionarias, lo que permite un movimiento suave.

### La tecnología
Usaremos el DOM del navegador para mostrar el juego, y leeremos la entrada del usuario manejando eventos clave.

El código relacionado con la pantalla y el teclado es solo una pequeña parte del trabajo que necesitamos hacer para construir este juego. Como todo parece cuadros de colores, dibujar es sencillo: creamos elementos DOM y usamos estilos para darles un color de fondo, tamaño y posición.

Podemos representar el fondo como una tabla, ya que es una cuadrícula de cuadrados que no cambia. Los elementos que se mueven libremente se pueden superponer utilizando elementos absolutamente posicionados.

En los juegos y otros programas que deben animar gráficos y responder a la entrada del usuario sin demoras apreciables, la eficiencia es importante. Aunque el DOM no fue diseñado originalmente para gráficos de alto rendimiento, en realidad es mejor de lo que cabría esperar. Viste algunas animaciones en capítulos anteriores . En una máquina moderna, un juego simple como este funciona bien, incluso si no nos preocupamos mucho por la optimización.

### Niveles
Queremos una forma legible y editable por humanos para especificar niveles. Dado que está bien que todo comience en una cuadrícula, podríamos usar cadenas grandes en las que cada carácter representa un elemento, ya sea una parte de la cuadrícula de fondo o un elemento en movimiento.

Los caracteres:
+ \# son paredes
+ @ es el jugador 
+ O son monedas 
+ = son bloques de lava
+ | es lava cayendo
+ . son puntos vacios

### Ejemplo de un nivel
```
................................................................................
................................................................................
................................................................................
................................................................................
................................................................................
................................................................................
..................................................................###...........
...................................................##......##....##+##..........
....................................o.o......##..................#+++#..........
.................................................................##+##..........
...................................#####..........................#v#...........
............................................................................##..
..##......................................o.o................................#..
..#.....................o....................................................#..
..#......................................#####.............................o.#..
..#..........####.......o....................................................#..
..#..@.......#..#................................................#####.......#..
..############..###############...####################.....#######...#########..
..............................#...#..................#.....#....................
..............................#+++#..................#+++++#....................
..............................#+++#..................#+++++#....................
..............................#####..................#######....................
................................................................................
................................................................................
```
En este ejemplo tenemos la mayoria de elementos que ofrece el mapa y como se estructuran estos niveles. Es decir, podemos maquetarlos de una manera rápida, pulcra y organizada.
